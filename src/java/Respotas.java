/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sala304b
 */
public class Respotas {
private int id;
private String respostas;
private String letra;
private Boolean correta;
private String idpergunta;

    public Respotas(int id, String respostas, String letra, Boolean correta, String idpergunta) {
        this.id = id;
        this.respostas = respostas;
        this.letra = letra;
        this.correta = correta;
        this.idpergunta = idpergunta;
    }

    public Respotas() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRespostas() {
        return respostas;
    }

    public void setRespostas(String respostas) {
        this.respostas = respostas;
    }

    public String getLetra() {
        return letra;
    }

    public void setLetra(String letra) {
        this.letra = letra;
    }

    public Boolean isCorreta() {
        return correta;
    }

    public void setCorreta(Boolean correta) {
        this.correta = correta;
    }

    public String getIdpergunta() {
        return idpergunta;
    }

    public void setIdpergunta(String idpergunta) {
        this.idpergunta = idpergunta;
    }

    
}
