/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sala304b
 */
public class Jogador {
    private String nome;
    private String sobrenome;
    private int numerojogador;
    private int id;

    public Jogador() {
    }

    public Jogador(String nome, String sobrenome, int numerojogador, int id) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.numerojogador = numerojogador;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public int getNumerojogador() {
        return numerojogador;
    }

    public void setNumerojogador(int numerojogador) {
        this.numerojogador = numerojogador;
    }
    
    
    
}
